# gackup

This tool helps to mirror one repository to an other.

With the help of the _install.sh_ script you can deploy the script and the systemd service and timer file.

## Installation

```bash
git clone https://codeberg.org/cinux/gackup
sudo bash install.sh
sudo systemctl enable gackup.timer
sudo systemctl start gackup.timer 
```

## Configuration
```
- name: codeberg dotfiles
  source_url: https://codeberg.org/cinux/dotfiles
  target_url: git@github.com:<myuser>/<my-fancy-repository>.git
- name: codeberg gackup
  source_url: https://codeberg.org/cinux/gackup
  target_url: git@github.com:<myuser>/<my-fancy-repository>.git
```
| Name | Description |
| :--- | :--- |
| name | Expressive name, can be freely chosen |
| source_url | Source url to copy from |
| target_url | Destination Url to be copied to |
