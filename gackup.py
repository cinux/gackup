import os
from git import Repo
import tempfile
import shutil
import yaml
import argparse

parse = argparse.ArgumentParser(description='Pull from source_url and push to target_url')
parse.add_argument('--path', type=str, help='Path to the yml file',required=False)

args = parse.parse_args()
TMPDIR = tempfile.mkdtemp()
print(TMPDIR)


def pull(source_url,repo_path):
    return Repo.clone_from(source_url,repo_path)

def push_all(repo,target_url,branchs):
    print('Nun erfolg der push_all')
    repo.create_remote('upload',target_url)
    repo.remotes[0].fetch()
    ssh_cmd = 'ssh -i $HOME/.ssh/id_rsa'
    with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
        for branch in branchs:
            print("Push " + branch)
            repo.git.checkout(branch.replace('remotes/origin/','')) 
            repo.remotes.upload.push(branch.replace('remotes/origin/',''))

def getAllRemoteBranches():
    repo.git.fetch(all=True)
    exclusions = [ 
                'remotes/origin/HEAD',
                '* master',
                '  remotes/origin/HEAD -> origin/master' ]
    branchs = []
    for branch in repo.git.branch('-a').split("\n"):
        if not branch in exclusions:
            branchs.append(branch.replace('remotes/origin/','').lstrip())
    return branchs

def readYaml(file):
    with open(file,'r') as file:
        content = yaml.safe_load(file)
    return content

try:
    if not args.path in globals():
        items = readYaml(args.path)
    else:
        items = '/etc/gackup-list.yml'
    for item in items:
        print('Bearbeite ' + item['name'])
        item['repo_path'] =  TMPDIR + "/" + item['name'].replace(" ", "_")
        repo = pull(item['source_url'],item['repo_path'])
        repo.remotes.origin.fetch()
        push_all(repo,item['target_url'],getAllRemoteBranches())
except Exception as e:
    print(e)
    shutil.rmtree(TMPDIR)
if os.path.isdir('new_folder'):
    shutil.rmtree(TMPDIR)
