#!/bin/bash

echo "Setup Software"
cp gackup.py /usr/local/bin/gackup.py
chmod +x /usr/local/bin/gackup.py
cp gackup-list.yml /etc/
echo "Setup Service and timer"
cp gackup.service /etc/systemd/system/
cp gackup.timer /etc/systemd/system/
